// main view model
function ConstructorViewModel(unitData = {}, apiData = {}, uploader = {}) {
  var self = this;
  // static variables
  self.apiData = apiData;
  self.uploader = uploader;

  self.initData = function () {
    self.businessTargets = ko.observableArray(unitData.businessTargets.map(function (businessTarget) {
      return {
        title: ko.observable(businessTarget.title),
      }
    }));
    self.trainingTarget = ko.observable(unitData.trainingTarget);
    self.stagesList = ko.observableArray(unitData.stagesList.map(function (stage) {
      return {
        visitStageTitle: ko.observable(stage.visitStageTitle),
        visitStageSkills: ko.observableArray(stage.visitStageSkills.map(function (skill) {
          return {
            skillTitle: ko.observable(skill.skillTitle),
            description: ko.observable(skill.description),
          }
        }))
      }
    }));
  }

  self.onInit = function () {
    console.log(data);
    if (data.unitData.trainingTarget || data.unitData.businessTargets || data.unitData.stagesList) {
      self.initData()
    } else {
      self.businessTargets = ko.observableArray([]);
      self.trainingTarget = ko.observable();
      self.stagesList = ko.observableArray();
    }
    // pushing templates
    self.businessTargets.push({ title: ko.observable("") });
    self.stagesList.push({
      visitStageTitle: ko.observable("Новый этап"),
      visitStageSkills: ko.observableArray([])
    });
    self.stagesList().forEach(stage => {
      stage.visitStageSkills.push({
        skillTitle: ko.observable(""),
        description: ko.observable(""),
      })
    });
  }

  self.addBusinessTargetIfLast = function () {
    if (self.businessTargets.indexOf(this) === self.businessTargets().length - 1) {
      self.businessTargets.push({ title: ko.observable("") });
    }
  }

  self.deleteBusinessTargetIfEmpty = function () {
    if (this.title().trim() === "") {
      self.businessTargets.remove(this);
    }
  }

  self.addVisitStageOrSkillIfLast = function (skill, stage) {
    const stageIndex = self.stagesList.indexOf(stage);
    const skillIndex = self.stagesList()[stageIndex].visitStageSkills.indexOf(skill)

    // first - check stage
    if (stageIndex === self.stagesList().length - 1) {
      self.stagesList.push({
        visitStageTitle: ko.observable("Новый этап"),
        visitStageSkills: ko.observableArray([
          { skillTitle: ko.observable(""), description: ko.observable("") }
        ])
      });
    }
    // chech skill
    if (skillIndex === self.stagesList()[stageIndex].visitStageSkills().length - 1) {
      self.stagesList()[stageIndex].visitStageSkills.push({
        skillTitle: ko.observable(""),
        description: ko.observable("")
      });
    }
  }

  self.deleteTemplateObjects = function () {
    self.businessTargets.pop()
    self.stagesList.pop()
    self.stagesList().forEach((stage) => {
      stage.visitStageSkills.pop();
    })
  }

  self.deleteVisitStageOrSkillIfEmpty = function (skill, stage) {
    const stageIndex = self.stagesList.indexOf(stage);
    var lastSkillInStage = false;
    // check skill
    if (skill.skillTitle().trim() === "" && skill.description().trim() === "") {
      self.stagesList()[stageIndex].visitStageSkills.remove(skill);
      lastSkillInStage = stage.visitStageSkills().length === 1 || false;
    }
    // check stage
    if (lastSkillInStage) {
      self.stagesList.remove(stage);
    }
  }

  self.buildJSON = () => {
    var data = {}
    data.trainingTarget = this.trainingTarget();
    data.stagesList = this.stagesList().map(stage => {
      return {
        visitStageTitle: stage.visitStageTitle(),
        visitStageSkills: stage.visitStageSkills().map(skill => {
          return {
            skillTitle: skill.skillTitle(),
            description: skill.description()
          }
        })
      }
    })
    data.businessTargets = this.businessTargets().map(target => {
      return { title: target.title() }
    });
    return data;
  }

  self.makeStageTitleEditable = function (c, event) {
    event.target.setAttribute('contentEditable', 'true');
  }

  self.makeStageTitleNotEditable = function (c, event) {
    event.target.setAttribute('contentEditable', 'false');
  }

}

var data = {
  unitData: {
    trainingName: "Jonathan Drake",
    trainingTarget: "Cagmikhen",
    businessTargets: [
      { title: "Rosetta McKenzie" },
      { title: "Mike Gutierrez" },
      { title: "Christina McDaniel" },
      { title: "Amelia Little" },
      { title: "Ricky McGuire" },
      { title: "Dale Terry" }
    ],
    stagesList: [
      {
        visitStageTitle: "stage 1",
        visitStageSkills: [
          { skillTitle: "skill 1", description: "krasava" },
          { skillTitle: "skill 1", description: "krasava" },
          { skillTitle: "skill 1", description: "krasava" },
          { skillTitle: "skill 1", description: "krasava" },
        ]
      },
      {
        visitStageTitle: "stage 2",
        visitStageSkills: [
          { skillTitle: "skill 1", description: "krasava" },
          { skillTitle: "skill 2", description: "krasava" }
        ]
      },
      {
        visitStageTitle: "stage 3",
        visitStageSkills: [
          { skillTitle: "skill 1", description: "krasava" },
          { skillTitle: "skill 2", description: "krasava" },
          { skillTitle: "skill 3", description: "krasava" }
        ]
      },
    ]
  }
}

// html content-editable binding
ko.bindingHandlers.editableHTML = {
  init: function (element, valueAccessor) {
    var $element = $(element);
    var initialValue = ko.utils.unwrapObservable(valueAccessor());
    $element.html(initialValue);
    $element.on('keyup', function () {
      observable = valueAccessor();
      observable($element.html());
    });
  }
};

//функция вызывается со стороны ТС
var init = () => {
  console.log('init called')
  this.viewModel = new ConstructorViewModel(this.data.unitData, this.data.apiData, this.data.uploader);
  this.viewModel.onInit();
  ko.applyBindings(this.viewModel);
}

//функция вызывается со стороны ТС
var submit = async () => {
  this.viewModel.deleteTemplateObjects();
  return await this.viewModel.buildJSON();
}

