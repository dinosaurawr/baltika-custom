function TrainerDataViewModel(resulData) {
  this.businessTargets = resulData.businessTargets;
  this.trainingName = resulData.trainingName;
  this.trainingTarget = resulData.trainingTarget;
  this.geopozitions = resulData.geopozitions;
  this.stagesList = resulData.stagesList;
} 

var data = {
  resulData: {
    businessTargets: [
      {title: "Jim Rodriguez", onStartDay: 50, onEndDay: 41},
      {title: "Mittie Murphy", onStartDay: 50, onEndDay: 1},
      {title: "Claudia Ryan", onStartDay: 50, onEndDay: 37},
      {title: "Chris Harrison", onStartDay: 50, onEndDay: 43},
      {title: "Leon Romero", onStartDay: 50, onEndDay: 5},
    ],
    trainingTarget: "Nicholas Carroll",
    geopozitions: [
      {title: "Bessie Burke", address:"Caewmo", time: "10:05"},
      {title: "Bessie Burke", address:"Caewmo", time: "11:05"},
      {title: "Bessie Burke", address:"Caewmo", time: "12:05"},
      {title: "Bessie Burke", address:"Caewmo", time: "14:05"},
      {title: "Bessie Burke", address:"Caewmo", time: "15:05"},
      {title: "Bessie Burke", address:"Caewmo", time: "18:05"},
    ],
    stagesList: [
      {
        visitStageTitle: "Augusta Burton",
        visitStageSkills: [
          {skillTitle: "Roxie Murphy", behavior: "Stephen Roberson", status: true},
          {skillTitle: "Loretta Turner", behavior: "Adam Cook", status: true},
          {skillTitle: "Rosie Cole", behavior: "Thomas Pittman", status: false},
          {skillTitle: "Theresa Bell", behavior: "Marcus Jefferson", status: true},
          {skillTitle: "Amy Byrd", behavior: "Louisa Benson", status: true},
          {skillTitle: "Hallie Tyler", behavior: "Lydia Fields", status: false},
        ]
      },
      {
        visitStageTitle: "Augusta Burton",
        visitStageSkills: [
          {skillTitle: "Roxie Murphy", behavior: "Stephen Roberson", status: true},
          {skillTitle: "Loretta Turner", behavior: "Adam Cook", status: true},
          {skillTitle: "Rosie Cole", behavior: "Thomas Pittman", status: false},
          {skillTitle: "Theresa Bell", behavior: "Marcus Jefferson", status: true},
          {skillTitle: "Amy Byrd", behavior: "Louisa Benson", status: true},
          {skillTitle: "Hallie Tyler", behavior: "Lydia Fields", status: false},
        ]
      },
      {
        visitStageTitle: "Augusta Burton",
        visitStageSkills: [
          {skillTitle: "Roxie Murphy", behavior: "Stephen Roberson", status: true},
          {skillTitle: "Loretta Turner", behavior: "Adam Cook", status: true},
          {skillTitle: "Rosie Cole", behavior: "Thomas Pittman", status: false},
          {skillTitle: "Theresa Bell", behavior: "Marcus Jefferson", status: true},
          {skillTitle: "Amy Byrd", behavior: "Louisa Benson", status: true},
          {skillTitle: "Hallie Tyler", behavior: "Lydia Fields", status: false},
        ]
      },
      {
        visitStageTitle: "Augusta Burton",
        visitStageSkills: [
          {skillTitle: "Roxie Murphy", behavior: "Stephen Roberson", status: true},
          {skillTitle: "Loretta Turner", behavior: "Adam Cook", status: true},
          {skillTitle: "Rosie Cole", behavior: "Thomas Pittman", status: false},
          {skillTitle: "Theresa Bell", behavior: "Marcus Jefferson", status: true},
          {skillTitle: "Amy Byrd", behavior: "Louisa Benson", status: true},
          {skillTitle: "Hallie Tyler", behavior: "Lydia Fields", status: false},
        ]
      },
    ]
  }
}

var self = this;

function init() {
  self.viewModel = new TrainerDataViewModel(data.resulData);
  ko.applyBindings(self.viewModel);
}

function submit() {

}